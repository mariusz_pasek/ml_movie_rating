import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.neural_network import MLPRegressor
from sklearn.metrics import mean_absolute_error
import eli5
from eli5.sklearn import PermutationImportance

import utils

utils.initialize()

merged_table = pd.read_csv('data/nn_preprocessed.csv')

X_train, X_test, y_train, y_test = train_test_split(merged_table.drop('rating', axis=1), merged_table['rating'],
                                                    test_size=0.20, random_state=1)


# regr = RandomForestRegressor(bootstrap=True,
#                              criterion='mse',
#                              max_depth=800,
#                              max_features='auto',
#                              max_leaf_nodes=None,
#                              min_impurity_decrease=0.0,
#                              min_impurity_split=None,
#                              min_samples_leaf=5,
#                              min_samples_split=2,
#                              min_weight_fraction_leaf=0.0,
#                              n_estimators=300,
#                              n_jobs=8,
#                              oob_score=True,
#                              random_state=0,
#                              verbose=0,
#                              warm_start=False)
#
regr = MLPRegressor(hidden_layer_sizes=(32, 32, 32),
                    activation="relu",
                    solver='adam',
                    alpha=0.0001,
                    batch_size='auto',
                    learning_rate="constant",
                    learning_rate_init=0.001,
                    max_iter=200,
                    shuffle=True,
                    random_state=None,
                    early_stopping=False,
                    n_iter_no_change=10)

regr.fit(X_train, y_train)
print('Score', regr.score(X_test, y_test))
y_predicted = regr.predict(X_test)
print('MAE', mean_absolute_error(y_predicted, y_test))

perm = PermutationImportance(regr, random_state=1).fit(X_test, y_test)
print(eli5.format_as_text(eli5.explain_weights(perm, top=100, feature_names=X_test.columns.tolist())))

html_object = eli5.show_weights(perm, feature_names=X_test.columns.tolist())
with open('html_file.html', 'w') as f:
    f.write(html_object.data)
