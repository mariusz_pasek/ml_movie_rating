import pandas as pd


def initialize():
    pd.set_option('display.max_rows', 500)
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 1000)


def merge_tables():
    movies_data = pd.read_csv('data/movies.csv')
    ratings_data = pd.read_csv('data/ratings.csv')

    genres = set()
    genres_per_movie = {}

    for _, movies_row in movies_data.iterrows():
        genres_in_movie = movies_row['genres'].split('|')
        genres.update(genres_in_movie)
        genres_per_movie[str(movies_row['movieId'])] = set(genres_in_movie)

    for genre in genres:
        genre_table = pd.DataFrame(data=[0] * len(ratings_data.index), columns=[genre])
        ratings_data = pd.concat([ratings_data, genre_table], axis=1)

    for idx, ratings_row in ratings_data.iterrows():
        genres_list = genres_per_movie[str(int(ratings_row['movieId']))]
        for genre in genres_list:
            ratings_row[genre] = 1
        ratings_data.loc[idx] = ratings_row

    ratings_data.to_csv('data/merged_ratings.csv')


def nn_preprocess():
    merged_table = pd.read_csv('data/merged_ratings.csv')
    merged_table = merged_table.drop(['Unnamed: 0', 'timestamp', '(no genres listed)'], axis=1)

    merged_table = pd.get_dummies(merged_table, columns=['userId', 'movieId'])

    merged_table['rating'] /= 2.5
    merged_table['rating'] -= 1
    merged_table.to_csv('data/nn_preprocessed.csv')
